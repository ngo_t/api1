FROM php:7-apache

RUN a2enmod rewrite

RUN chown -R www-data:www-data /var/www

COPY start-apache /usr/local/bin

RUN ["chmod", "+x", "/usr/local/bin/start-apache"]

RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-enable pdo_mysql

CMD ["start-apache"]